import React from 'react';
import './App.css';
import Lists from './tugas13/tugas11'
import Timer from './tugas12/countdown'
import DateTime from './tugas12/time'

let dataHargaBuah = [
  {nama: "Semangka", harga: 10000, berat: 1000},
  {nama: "Anggur", harga: 40000, berat: 500},
  {nama: "Strawberry", harga: 30000, berat: 400},
  {nama: "Jeruk", harga: 30000, berat: 1000},
  {nama: "Mangga", harga: 30000, berat: 500}
]

function App() {
  return (
    <div className="App">
      <h1>Tabel Harga Buah</h1>
      <Lists headers={["Nama", "Harga", "Berat", "Aksi"]} contents={dataHargaBuah} />
      <Timer start="101"/>
      <DateTime />
    </div>
  );
}

export default App;
