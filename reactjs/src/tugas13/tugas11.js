import React from 'react'

// class TableHeader extends React.Component {
//     render() {
//         return (
//         <thead>
//             <tr>
//                 {this.props.headers.map(el => {
//                     return (
//                         <th>
//                             {el}
//                         </th>
//                     )
//                 })}
//             </tr>
//         </thead>)
//     }
// }

// class TableContent extends React.Component {
//     render() {
//         return (
//         <>
//             {this.props.contents.map(row => {
//                 return (
//                     <tr>
//                         <td>{row.nama}</td>
//                         <td>{row.harga}</td>
//                         <td>{row.berat/1000} kg</td>
//                     </tr>
//                 )
//             })}
//         </>
//         )
//     }
// }

class Lists extends React.Component{
    constructor(props){
        super(props)
        this.state ={
            daftarBuah : [],
            buah: {
                nama: "",
                harga: 0,
                berat: 0,
            }
        }
    
        this.namaChange = this.namaChange.bind(this);
        this.hargaChange = this.hargaChange.bind(this);
        this.beratChange = this.beratChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount(){
        if (this.props.contents !== undefined){
          this.setState({daftarBuah: this.props.contents})
        }
      }
  
    namaChange(event){
      this.setState({buah:{
          nama:event.target.value,
          harga: this.state.buah.harga,
          berat: this.state.buah.berat,
      }});
    }
  
    hargaChange(event){
      this.setState({buah:{
          nama: this.state.buah.nama,
          harga:event.target.value,
          berat: this.state.buah.berat,
      }});
    }
  
    beratChange(event){
      this.setState({buah:{
        nama: this.state.buah.nama,
        harga: this.state.buah.harga,
        berat:event.target.value
      }});
    }
  
    handleSubmit(event){
      event.preventDefault()
      console.log(this.state.buah)
      this.setState({
        daftarBuah: [...this.state.daftarBuah, this.state.buah],
        buah: {
            nama: "",
            harga: 0,
            berat: 0,
        }
      })
    }
  
    render(){
        return(
            <>
            <table>
                <thead>
                    <tr>
                        {this.props.headers.map(el => {
                            return (
                            <th>{el}</th>
                            )
                        })}
                    </tr>
                </thead>
                <tbody>
                    {
                    this.state.daftarBuah.map((val, index)=>{
                        return(                    
                        <tr>
                            <td>{val.nama}</td>
                            <td>{val.harga}</td>
                            <td>{val.berat/1000} kg</td>
                        </tr>
                        )
                    })
                    }
                </tbody>
            </table>
            {/* Form */}
            <h1>Tambah Data</h1>
            <form onSubmit={this.handleSubmit}>
                <label> Nama buah: </label>          
                <input type="text" name="nama" value={this.state.buah.nama} onChange={this.namaChange}/><br/>
                <label> Harga buah: </label>          
                <input type="text" name="harga" value={this.state.buah.harga} onChange={this.hargaChange}/><br/>
                <label> Berat buah (gram): </label>          
                <input type="text" name="berat" value={this.state.buah.berat} onChange={this.beratChange}/><br/>
                <button>submit</button>
            </form>
            </>
        )
    }
}

export default Lists