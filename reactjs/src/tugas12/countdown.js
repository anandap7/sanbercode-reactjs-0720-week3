import React, {Component} from 'react'

class Timer extends Component{
  constructor(props){
    super(props)
    this.state = {
      time: 0
    }
  }

  componentDidMount(){
    if (this.props.start !== undefined){
      this.setState({time: this.props.start})
    }
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount(){
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      time: this.state.time - 1
    });
  }


  render(){
    if(this.state.time === 0) {
      clearInterval(this.timerID)
      return null
    } else {
      return(
        <>
          <h3 style={{textAlign: "center"}}>
            hitung mundur: {this.state.time}
          </h3>
        </>
      )
    }
  }
}

export default Timer