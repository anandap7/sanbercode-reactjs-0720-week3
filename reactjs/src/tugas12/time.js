import React, {Component} from 'react'
import Time from 'react-time'

class DateTime extends Component {
    constructor(props) {
        super(props)
        this.state = {
            now: new Date()
        }
    }

    componentDidMount(){
        if (this.props.start !== undefined){
            this.setState({now: this.props.start})
        }
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }

    componentWillUnmount(){
        clearInterval(this.timerID);
    }

    tick() {
        this.setState({
            now: new Date()
        });
    }


    render(){
        return(
            <>
            <h3 style={{textAlign: "center"}}>
                sekarang jam <Time value={this.state.now} format="HH:mm:ss" />
            </h3>
            </>
        )
    }
}

export default DateTime